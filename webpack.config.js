const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const deployFolder = 'deploy/';

module.exports = (env) => {
    return {
        mode: env.development ? 'development' : 'production',
        entry: './src/scripts/index.js',
        output: {
            filename: 'main.js',
            path: path.resolve(__dirname, deployFolder)
        },
        devtool: 'inline-source-map',
        optimization: {
            minimize: !env.development,
			splitChunks: {
				chunks: 'all'
			}
		},
        plugins: [
            new CleanWebpackPlugin({ 
                dry: true,
                // root: path.resolve(__dirname, './'),
                verbose: env.development // write console logs
            }),
            new CopyWebpackPlugin([{ 
                from: path.resolve(__dirname, './src/assets'), 
                to: 'assets' 
            }]),
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, './src/index.html')
            }),
            new MiniCssExtractPlugin()
        ],
        module: {
            rules: [{
                test: /\.(js)$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }, {
                test: /\.(sa|sc|c)ss$/,
                use: env.development ? ['style-loader'] : [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },  {
                test: /\.(otf|woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: './assets/fonts/'
                    }
                }]
            }]
        }
    }
};